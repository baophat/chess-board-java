package self.chess.core;

import self.chess.chesspiece.european.ChessPiece;
import self.chess.chesspiece.european.KingPiece;

public class Chess implements ChessGame {
    // store the side to move this turn
    ChessGame.Side properSide = ChessGame.Side.NORTH;

    // Check if it is legal to place a given piece
    public boolean legalPieceToPlay(ChessPiece piece, int row, int column) {
        return piece.getSide() == properSide;
    }

    // make move for chess piece
    public boolean makeMove(ChessPiece piece, int toRow, int toColumn) {

        if (legalPieceToPlay(piece, piece.getRow(), piece.getColumn())) {
            if (piece instanceof KingPiece
                    && piece.getFirstMoveStatus()
                    && ((KingPiece) piece).isMoveToCastleCell(toRow, toColumn)
            ) {
                if (((KingPiece) piece).processCastleMove(toRow, toColumn)) {
                    changeTurn();
                    return true;
                } else return false;
            } else if (piece.isLegalMove(toRow, toColumn)) {
                piece.moveDone(toRow, toColumn);
                changeTurn();
                return true;
            }
        }
        return false;
    }

    public boolean canChangeSelection(ChessPiece piece, int row, int column) {
        return true;
    }

    /**
     * Return the number of rows
     */
    public int getNumRows() {
        return 0;
    }

    ;

    /**
     * Return the number of columns
     */
    public int getColumns() {
        return 0;
    }

    ;

    /**
     * Place all pieces on the board and start the game
     *
     * @param board: the chessboard to place pieces on
     */
    public void startGame(ChessBoard board) {
    }

    ;

    private void changeTurn() {
        if (properSide == ChessGame.Side.SOUTH) {
            properSide = ChessGame.Side.NORTH;
        } else {
            properSide = ChessGame.Side.SOUTH;
        }
    }
}
