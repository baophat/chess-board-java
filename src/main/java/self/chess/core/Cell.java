package self.chess.core;

public class Cell implements Comparable<Cell> {

    private int row;
    private int col;

    public Cell() {
    }

    public Cell(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "row=" + row +
                ", col=" + col +
                '}';
    }

    // implement compareTo
    public int compareTo(Cell cell) {
        if ((cell.getRow() == this.getRow()) && (cell.getCol() == this.getCol())) return 0;
        else if ((cell.getRow() > this.getRow()) && (cell.getCol() > this.getCol())) return 1;
        else return -1;
    }
}
