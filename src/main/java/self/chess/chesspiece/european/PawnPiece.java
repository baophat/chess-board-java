package self.chess.chesspiece.european;

import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;

public class PawnPiece extends ChessPiece {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();
    // firstMove check if this is the first move of the piece
    private boolean firstMove = true;

    //constructor
    public PawnPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // check first move status
    public boolean getFirstMoveStatus() {
        return this.firstMove;
    }

    // set first move status
    public void setFirstMoveStatus(boolean status) {
        this.firstMove = status;
    }

    // upgrade to another piece if a pawn reach the other end of the board
    public void upgrade(int toRow, int toCol) {

        // the new piece that will replace the pawn
        ChessPiece upgradePiece;
        String upgradePieceString = javax.swing.JOptionPane.showInputDialog("Enter your upgrade choice: Queen / Knight / Bishop / Rook");
        switch (upgradePieceString) {
            case "Knight":
                upgradePiece = new KnightPiece(this.getSide(), EuropeanPieceType.N.name, this.getChessBoard());
                break;
            case "Bishop":
                upgradePiece = new BishopPiece(this.getSide(), EuropeanPieceType.B.name, this.getChessBoard());
                break;
            case "Rook":
                upgradePiece = new RookPiece(this.getSide(), EuropeanPieceType.R.name, this.getChessBoard());
                break;
            default:
                upgradePiece = new QueenPiece(this.getSide(), EuropeanPieceType.Q.name, this.getChessBoard());
                break;
        }
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
        this.getChessBoard().addPiece(upgradePiece, toRow, toCol);
    }

    // stores the possible move
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.clear();
        // check move for black side
        if (this.getSide() == ChessGame.Side.NORTH) {
            if (row == 0)
                return possibleMoves;
            if (this.getChessBoard().getPiece(row - 1, column) == null) {
                possibleMoves.add(new Cell(row - 1, column));
                if (row == 6) {
                    if (this.getChessBoard().getPiece(4, column) == null)
                        possibleMoves.add(new Cell(4, column));
                }
            }
            if ((column > 0) && (this.getChessBoard().getPiece(row - 1, column - 1) != null)
                    && (this.getChessBoard().getPiece(row - 1, column - 1).getSide() != this.getSide()))
                possibleMoves.add(new Cell(row - 1, column - 1));
            if ((column < 7) && (this.getChessBoard().getPiece(row - 1, column + 1) != null)
                    && (this.getChessBoard().getPiece(row - 1, column + 1).getSide() != this.getSide()))
                possibleMoves.add(new Cell(row - 1, column + 1));
        }

        // check move for white side
        else {
            if (row == 8)
                return possibleMoves;
            if (this.getChessBoard().getPiece(row + 1, column) == null) {
                possibleMoves.add(new Cell(row + 1, column));
                if (row == 1) {
                    if (this.getChessBoard().getPiece(3, column) == null)
                        possibleMoves.add(new Cell(3, column));
                }
            }
            if ((column > 0) && (this.getChessBoard().getPiece(row + 1, column - 1) != null)
                    && (this.getChessBoard().getPiece(row + 1, column - 1).getSide() != this.getSide()))
                possibleMoves.add(new Cell(row + 1, column - 1));
            if ((column < 7) && (this.getChessBoard().getPiece(row + 1, column + 1) != null)
                    && (this.getChessBoard().getPiece(row + 1, column + 1).getSide() != this.getSide()))
                possibleMoves.add(new Cell(row + 1, column + 1));
        }
        return possibleMoves;
    }

    // check if a move is legal
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if ((possibleMove.compareTo(targetSquare) == 0)
                    && (super.isLegalMove(toRow, toColumn))) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    public boolean isLegalNonCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (!getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // show whether a capture move is valid
    public boolean isLegalCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // handle details after a move is done
    public void moveDone(int toRow, int toCol) {
        if ((this.isLegalMove(toRow, toCol))
                && (this.getChessBoard().hasPiece(toRow, toCol))
                && (this.getChessBoard().getPiece(toRow, toCol).getSide() != this.getSide()))
            super.getChessBoard().removePiece(toRow, toCol);

        this.getChessBoard().addPiece(new PawnPiece(this.getSide(), EuropeanPieceType.P.name, this.getChessBoard()), toRow, toCol);

        ((PawnPiece) this.getChessBoard().getPiece(toRow, toCol)).setFirstMoveStatus(false);

        this.getChessBoard().removePiece(this.getRow(), this.getColumn());

        if ((toRow == 0) || (toRow == 7))
            this.upgrade(toRow, toCol);
    }
}
