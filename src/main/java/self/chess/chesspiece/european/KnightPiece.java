package self.chess.chesspiece.european;

import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KnightPiece extends ChessPiece {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();

    //constructor
    public KnightPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // Return squares for valid move
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.clear();
        List<Cell> posCell = new ArrayList<>(
                Arrays.asList(
                        new Cell(row + 1, column - 2),
                        new Cell(row + 1, column + 2),
                        new Cell(row + 2, column - 1),
                        new Cell(row + 2, column + 1),
                        new Cell(row - 1, column - 2),
                        new Cell(row - 1, column + 2),
                        new Cell(row - 2, column - 1),
                        new Cell(row - 2, column + 1)
                )
        );
        for (Cell cell : posCell) {
            if ((cell.getRow() >= 0)
                    && (cell.getRow() < 8)
                    && (cell.getCol() >= 0)
                    && (cell.getCol() < 8)
                    && (((board.getPiece(cell.getRow(), cell.getCol())) == null)
                    || (board.getPiece(cell.getRow(), cell.getCol()).getSide() != this.getSide())))
                possibleMoves.add(new Cell(cell.getRow(), cell.getCol()));
        }
        return possibleMoves;
    }

    // check if a move is legal
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if ((possibleMove.compareTo(targetSquare) == 0)
                    && (super.isLegalMove(toRow, toColumn))) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    public boolean isLegalNonCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (!getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // show whether a capture move is valid
    public boolean isLegalCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // handle details after a move is done
    public void moveDone(int row, int column) {
        if ((this.isLegalMove(row, column)) && (this.getChessBoard().hasPiece(row, column)) && (this.getChessBoard().getPiece(row, column).getSide() != this.getSide()))
            super.getChessBoard().removePiece(row, column);
        this.getChessBoard().addPiece(new KnightPiece(this.getSide(), EuropeanPieceType.N.name, this.getChessBoard()), row, column);
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }
}
