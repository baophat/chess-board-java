package self.chess.chesspiece.european;

import self.chess.chesspiece.xiangqi.XiangqiKingPiece;
import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;

public class ChessPiece {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();
    // side stores the side of the piece
    private final ChessGame.Side side;
    // label stores the label of the piece
    private final String label;
    // board stores the chess board this piece is on
    private final ChessBoard board;
    // row and column stores the position of the piece
    private int row;
    private int column;

    // constructor
    public ChessPiece(ChessGame.Side side, String label, ChessBoard board) {
        this.side = side;
        this.label = label;
        this.board = board;
    }

    // check first move status
    public boolean getFirstMoveStatus() {
        // firstMove check if this is the first move of the piece
        return true;
    }

    // show the side of piece
    public ChessGame.Side getSide() {
        return this.side;
    }

    // show the label of piece
    public String getLabel() {
        return this.label;
    }

    // show the icon of piece
    public Object getIcon() {
        return null;
    }

    // get location of piece on chess board
    public Cell getLocation() {
        return new Cell(this.row, this.column);
    }

    // set location of piece on chess board
    public void setLocation(int row, int column) {
        this.row = row;
        this.column = column;
    }

    // show whether the move to a new square is valid
    public boolean isLegalMove(int toRow, int toColumn) {
        return !((toRow >= getChessBoard().numRows()) | (toColumn >= getChessBoard().numColumns()));
    }

    // show the chess board this piece is on
    public ChessBoard getChessBoard() {
        return this.board;
    }

    // show the row this piece is on
    public int getRow() {
        return this.row;
    }

    // show the column this piece is on
    public int getColumn() {
        return this.column;
    }

    // show whether a non-capture move is valid
    public boolean isLegalNonCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column)) && (!getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // show whether a capture move is valid
    public boolean isLegalCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column)) && (getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // handle process after a move is done if necessary
    public void moveDone(int row, int column) {
    }

    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        return null;
    }

    // check if this is the only piece between two xiangqi kings
    public boolean kingFacingKing() {
        // count the empty rows in the column of this piece
        int countRows = 0;
        // int count the number of kings in column
        int countKings = 0;
        for (int i = 0; i < getChessBoard().numRows(); i++) {
            if (board.getPiece(i, this.getColumn()) instanceof XiangqiKingPiece) {
                countKings++;
            }
            if ((board.getPiece(i, this.getColumn()) == null)
                    || (board.getPiece(i, this.getColumn()) == this)) {
                countRows++;
            }
        }
        if ((countRows == getChessBoard().numRows() - 2) && (countKings == 2)) {
            return true;
        }
        return false;
    }
}
