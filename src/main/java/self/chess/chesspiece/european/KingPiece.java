package self.chess.chesspiece.european;

import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KingPiece extends ChessPiece {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();
    // dummy piece use to check if a square is threatened
    private final ChessPiece dummy = new ChessPiece(this.getSide(), "Dummy", this.getChessBoard());
    // firstMove check if this is the first move of the piece
    private boolean firstMove = true;

    // constructor
    public KingPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // check first move status
    public boolean getFirstMoveStatus() {
        return this.firstMove;
    }

    // set first move status
    public void setFirstMoveStatus(boolean status) {
        this.firstMove = status;
    }


    // return the right castle move
    public boolean processRightCastleMoveLegal(int toRow, int toCol) {
        // check if the king is checked
        if (this.getChessBoard().squareThreatened(toRow, this.getColumn(), this))
            return false;
        // check if there are anything between king and rook
        if ((!super.getChessBoard().hasPiece(toRow, toCol))
                && (!super.getChessBoard().hasPiece(toRow, toCol - 1))
                && (super.getChessBoard().hasPiece(toRow, toCol + 1))
                && (super.getChessBoard().getPiece(toRow, toCol + 1) instanceof RookPiece)
                && (super.getChessBoard().getPiece(toRow, toCol + 1).getFirstMoveStatus())) {

            // check if king moved square to the right is threatened
            this.getChessBoard().addPiece(dummy, toRow, toCol);
            if (this.getChessBoard().squareThreatened(toRow, toCol, dummy))
                return false;
            this.getChessBoard().removePiece(toRow, toCol);

            // check if rook moved square to the right is threatened
            this.getChessBoard().addPiece(dummy, toRow, toCol - 1);
            if (this.getChessBoard().squareThreatened(toRow, toCol - 1, dummy))
                return false;
            this.getChessBoard().removePiece(toRow, toCol - 1);

            // process moveDone
            this.getChessBoard().addPiece(new KingPiece(this.getSide(), EuropeanPieceType.K.name, this.getChessBoard()), toRow, toCol);
            ((KingPiece) this.getChessBoard().getPiece(toRow, toCol)).setFirstMoveStatus(false);
            this.getChessBoard().removePiece(toRow, toCol - 2);

            getChessBoard().addPiece(new RookPiece(getSide(), EuropeanPieceType.R.name, getChessBoard()), toRow, toCol - 1);
            ((RookPiece) getChessBoard().getPiece(toRow, toCol + 1)).setFirstMoveStatus(false);
            getChessBoard().removePiece(toRow, toCol + 1);

            return true;
        }
        return false;
    }

    // return the left castle move
    public boolean processLeftCastleMoveLegal(int toRow, int toCol) {
        // check if the king is checked
        if (this.getChessBoard().squareThreatened(toRow, this.getColumn(), this))
            return false;
        // check if there are anything between king and rook
        if ((!super.getChessBoard().hasPiece(toRow, toCol - 1))
                && (!super.getChessBoard().hasPiece(toRow, toCol))
                && (!super.getChessBoard().hasPiece(toRow, toCol + 1))
                && (super.getChessBoard().hasPiece(toRow, toCol - 2))
                && (super.getChessBoard().getPiece(toRow, toCol - 2) instanceof RookPiece)
                && (super.getChessBoard().getPiece(toRow, toCol - 2).getFirstMoveStatus())) {

            // check if first square to the left is threatened
            this.getChessBoard().addPiece(dummy, toRow, toCol - 1);
            if (this.getChessBoard().squareThreatened(toRow, toCol - 1, dummy))
                return false;
            this.getChessBoard().removePiece(toRow, toCol - 1);

            // check if second square to the left is threatened
            this.getChessBoard().addPiece(dummy, toRow, toCol);
            if (this.getChessBoard().squareThreatened(toRow, toCol, dummy))
                return false;
            this.getChessBoard().removePiece(toRow, toCol);

            // check if third square to the left is threatened
            this.getChessBoard().addPiece(dummy, toRow, toCol + 1);
            if (this.getChessBoard().squareThreatened(toRow, toCol + 1, dummy))
                return false;
            this.getChessBoard().removePiece(toRow, toCol + 1);

            // process moveDone
            this.getChessBoard().addPiece(new KingPiece(this.getSide(), EuropeanPieceType.K.name, this.getChessBoard()), toRow, toCol);
            ((KingPiece) this.getChessBoard().getPiece(toRow, toCol)).setFirstMoveStatus(false);
            this.getChessBoard().removePiece(toRow, toCol + 2);

            getChessBoard().addPiece(new RookPiece(getSide(), EuropeanPieceType.R.name, getChessBoard()), toRow, toCol + 1);
            ((RookPiece) getChessBoard().getPiece(toRow, toCol + 1)).setFirstMoveStatus(false);
            getChessBoard().removePiece(toRow, toCol - 2);

            return true;
        }
        return false;
    }

    public boolean processCastleMove(int toRow, int toCol) {
        if (isMoveToRightCastleCell(toRow, toCol)) return processRightCastleMoveLegal(toRow, toCol);
        else return processLeftCastleMoveLegal(toRow, toCol);
    }

    public boolean isMoveToCastleCell(int toRow, int toCol) {
        return isMoveToLeftCastleCell(toRow, toCol) || isMoveToRightCastleCell(toRow, toCol);
    }

    private boolean isMoveToRightCastleCell(int toRow, int toCol) {
        if (this.getSide() == ChessGame.Side.NORTH) {
            return toRow == 7 && toCol == 6;
        } else if (this.getSide() == ChessGame.Side.SOUTH) {
            return toRow == 0 && toCol == 6;
        }
        return false;
    }

    private boolean isMoveToLeftCastleCell(int toRow, int toCol) {
        if (this.getSide() == ChessGame.Side.NORTH) {
            return toRow == 7 && toCol == 2;
        } else if (this.getSide() == ChessGame.Side.SOUTH) {
            return toRow == 0 && toCol == 2;
        }
        return false;
    }

    // return moves available for piece
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.clear();
        List<Cell> posCell = new ArrayList<>(
                Arrays.asList(
                        new Cell(row, column - 1),
                        new Cell(row, column + 1),
                        new Cell(row + 1, column - 1),
                        new Cell(row + 1, column),
                        new Cell(row + 1, column + 1),
                        new Cell(row - 1, column - 1),
                        new Cell(row - 1, column),
                        new Cell(row - 1, column + 1)
                )
        );
        for (Cell cell : posCell) {
            if ((cell.getRow() >= 0)
                    && (cell.getRow() < 8)
                    && (cell.getCol() >= 0)
                    && (cell.getCol() < 8)
                    && (((board.getPiece(cell.getRow(), cell.getCol())) == null)
                    || (board.getPiece(cell.getRow(), cell.getCol()).getSide() != this.getSide())))
                possibleMoves.add(new Cell(cell.getRow(), cell.getCol()));
        }
        return possibleMoves;
    }

    // check if a move is legal
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if ((possibleMove.compareTo(targetSquare) == 0) && (super.isLegalMove(toRow, toColumn))) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    public boolean isLegalNonCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (!getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // show whether a capture move is valid
    public boolean isLegalCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // handle details after a move is done
    public void moveDone(int row, int column) {
        if ((this.isLegalMove(row, column))
                && (this.getChessBoard().hasPiece(row, column))
                && (this.getChessBoard().getPiece(row, column).getSide() != this.getSide()))
            super.getChessBoard().removePiece(row, column);

        this.getChessBoard().addPiece(new KingPiece(this.getSide(), EuropeanPieceType.K.name, this.getChessBoard()), row, column);

        ((KingPiece) this.getChessBoard().getPiece(row, column)).setFirstMoveStatus(false);

        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }
}
