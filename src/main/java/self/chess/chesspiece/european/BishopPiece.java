package self.chess.chesspiece.european;

import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;
import self.chess.moving.DiagonalMove;

import java.util.ArrayList;

public class BishopPiece extends ChessPiece implements DiagonalMove {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();

    //constructor
    public BishopPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // return squares for diagonal move
    public ArrayList<Cell> moveDiagonal(ChessBoard board, int row, int column) {
        possibleMoves.clear();
        int tempRow = row + 1, tempColumn = column - 1;
        while (tempRow < 8 && tempColumn >= 0) {
            if (board.getPiece(tempRow, tempColumn) == null) {
                possibleMoves.add(new Cell(tempRow, tempColumn));
            } else if (board.getPiece(tempRow, tempColumn).getSide() == this.getSide()) {
                break;
            } else {
                possibleMoves.add(new Cell(tempRow, tempColumn));
                break;
            }
            tempRow++;
            tempColumn--;
        }

        tempRow = row - 1;
        tempColumn = column + 1;
        while (tempRow >= 0 && tempColumn < 8) {
            if (board.getPiece(tempRow, tempColumn) == null) {
                possibleMoves.add(new Cell(tempRow, tempColumn));
            } else if (board.getPiece(tempRow, tempColumn).getSide() == this.getSide()) {
                break;
            } else {
                possibleMoves.add(new Cell(tempRow, tempColumn));
                break;
            }
            tempRow--;
            tempColumn++;
        }

        tempRow = row - 1;
        tempColumn = column - 1;
        while (tempRow >= 0 && tempColumn >= 0) {
            if (board.getPiece(tempRow, tempColumn) == null) {
                possibleMoves.add(new Cell(tempRow, tempColumn));
            } else if (board.getPiece(tempRow, tempColumn).getSide() == this.getSide()) {
                break;
            } else {
                possibleMoves.add(new Cell(tempRow, tempColumn));
                break;
            }
            tempRow--;
            tempColumn--;
        }

        tempRow = row + 1;
        tempColumn = column + 1;
        while (tempRow < 8 && tempColumn < 8) {
            if (board.getPiece(tempRow, tempColumn) == null) {
                possibleMoves.add(new Cell(tempRow, tempColumn));
            } else if (board.getPiece(tempRow, tempColumn).getSide() == this.getSide()) {
                break;
            } else {
                possibleMoves.add(new Cell(tempRow, tempColumn));
                break;
            }
            tempRow++;
            tempColumn++;
        }
        return possibleMoves;
    }

    // return all possible moves
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        return moveDiagonal(this.getChessBoard(), row, column);
    }

    // check if a move is legal
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if ((possibleMove.compareTo(targetSquare) == 0) && (super.isLegalMove(toRow, toColumn))) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    public boolean isLegalNonCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column)) && (!getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // show whether a capture move is valid
    public boolean isLegalCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column)) && (getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // handle details after a move is done
    public void moveDone(int row, int column) {
        if ((this.isLegalMove(row, column)) && (this.getChessBoard().hasPiece(row, column)) && (this.getChessBoard().getPiece(row, column).getSide() != this.getSide()))
            super.getChessBoard().removePiece(row, column);
        this.getChessBoard().addPiece(new BishopPiece(this.getSide(), EuropeanPieceType.B.name, this.getChessBoard()), row, column);
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }
}
