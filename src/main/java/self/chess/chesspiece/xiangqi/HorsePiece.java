package self.chess.chesspiece.xiangqi;

import self.chess.chesspiece.european.KnightPiece;
import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;

public class HorsePiece extends KnightPiece {
    // possibleMoves stores all possible move for the piece
    private ArrayList<Cell> possibleMoves = new ArrayList<>();

    // constructor
    public HorsePiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // Return squares for valid move
    @Override
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.clear();

        // check if this is the only piece between two xiangqi king
        if (super.kingFacingKing()) {
            return possibleMoves;
        }

        // check possible move similar to european chess
        possibleMoves = super.move(board, row, column);

        return possibleMoves;
    }

    /**
     * This checks if Horse has any pieces in its path
     *
     * @param row    is the row the piece is to be moved to
     * @param column is the column the piece is to be moved to
     * @return a boolean representing if the path is clear
     */
    public boolean isClearPath(int row, int column) {

        // checks the path for two squares on the far right
        if (column - this.getColumn() == 2) {
            return getChessBoard().getPiece(getRow(), getColumn() + 1) == null;
        }

        // checks the path for two squares on the far left
        else if (column - this.getColumn() == -2) {
            return getChessBoard().getPiece(getRow(), getColumn() - 1) == null;
        }

        // checks the path for two squares on the top
        else if (row - this.getRow() == 2) {
            return getChessBoard().getPiece(getRow() + 1, getColumn()) == null;
        }

        // checks the path for two squares at the bottom
        else if (row - this.getRow() == -2) {
            return getChessBoard().getPiece(getRow() - 1, getColumn()) == null;
        } else {
            return false;
        }
    }

    // check if a move is legal
    @Override
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if (possibleMove.compareTo(targetSquare) == 0
                    && isClearPath(toRow, toColumn)
                    && super.isLegalMove(toRow, toColumn)) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    @Override
    public boolean isLegalNonCaptureMove(int row, int column) {
        return this.isLegalMove(row, column)
                && !getChessBoard().hasPiece(row, column);
    }

    // show whether a capture move is valid
    @Override
    public boolean isLegalCaptureMove(int row, int column) {
        return this.isLegalMove(row, column)
                && getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column));
    }

    // handle details after a move is done
    @Override
    public void moveDone(int row, int column) {
        if (this.isLegalMove(row, column)
                && this.getChessBoard().hasPiece(row, column)
                && this.getChessBoard().getPiece(row, column).getSide() != this.getSide())
            super.getChessBoard().removePiece(row, column);
        this.getChessBoard().addPiece(new HorsePiece(this.getSide(), XiangQiPieceType.H.name, this.getChessBoard()), row, column);
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }
}
