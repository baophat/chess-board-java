package self.chess.chesspiece.xiangqi;

import self.chess.chesspiece.european.ChessPiece;
import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ElephantPiece extends ChessPiece {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();

    // constructor
    public ElephantPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // return square for valid move
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.clear();

        // check if this is the only piece between two xiangqi king
        if (super.kingFacingKing()) {
            return possibleMoves;
        }

        List<Cell> posCell = new ArrayList<>(
                Arrays.asList(
                        new Cell(row - 2, column - 2),
                        new Cell(row - 2, column + 2),
                        new Cell(row + 2, column - 2),
                        new Cell(row + 2, column + 2)
                )
        );
        for (Cell cell : posCell) {
            if (cell.getRow() >= 0
                    && cell.getRow() < board.numRows()
                    && cell.getCol() >= 0
                    && cell.getCol() < board.numColumns()
                    && (board.getPiece(cell.getRow(), cell.getCol()) == null
                    || board.getPiece(cell.getRow(), cell.getCol()).getSide() != this.getSide())) {
                // South side
                if (this.getSide() == ChessGame.Side.SOUTH && cell.getRow() < 5) {
                    possibleMoves.add(new Cell(cell.getRow(), cell.getCol()));
                }
                // North side
                if (this.getSide() == ChessGame.Side.NORTH && cell.getRow() > 4) {
                    possibleMoves.add(new Cell(cell.getRow(), cell.getCol()));
                }
            }
        }
        return possibleMoves;
    }

    /**
     * This checks if Elephant has any pieces in its path
     *
     * @param row    is the row the piece is to be moved to
     * @param column is the column the piece is to be moved to
     * @return a boolean representing if the path is clear
     */
    public boolean isClearPath(int row, int column) {

        // checks the path for the top right move
        if (column - this.getColumn() == 2 && row - this.getRow() == 2) {
            return getChessBoard().getPiece(getRow() + 1, getColumn() + 1) == null;
        }

        // checks the path for the bottom right move
        else if (column - this.getColumn() == 2 && row - this.getRow() == -2) {
            return getChessBoard().getPiece(getRow() - 1, getColumn() + 1) == null;
        }

        // checks the path for the top left move
        else if (column - this.getColumn() == -2 && row - this.getRow() == 2) {
            return getChessBoard().getPiece(getRow() + 1, getColumn() - 1) == null;
        }

        // checks the path for the bottom left move
        else if (column - this.getColumn() == -2 && row - this.getRow() == -2) {
            return getChessBoard().getPiece(getRow() - 1, getColumn() - 1) == null;
        } else {
            return false;
        }
    }

    // check if a move is legal
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if (possibleMove.compareTo(targetSquare) == 0
                    && isClearPath(toRow, toColumn)
                    && super.isLegalMove(toRow, toColumn)) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    public boolean isLegalNonCaptureMove(int row, int column) {
        return this.isLegalMove(row, column)
                && !getChessBoard().hasPiece(row, column);
    }

    // show whether a capture move is valid
    public boolean isLegalCaptureMove(int row, int column) {
        return this.isLegalMove(row, column)
                && getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column));
    }

    // handle details after a move is done
    public void moveDone(int row, int column) {
        if (this.isLegalMove(row, column)
                && this.getChessBoard().hasPiece(row, column)
                && this.getChessBoard().getPiece(row, column).getSide() != this.getSide())
            super.getChessBoard().removePiece(row, column);
        this.getChessBoard().addPiece(
                new ElephantPiece(this.getSide(), XiangQiPieceType.E.name, this.getChessBoard()), row, column);
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }
}
