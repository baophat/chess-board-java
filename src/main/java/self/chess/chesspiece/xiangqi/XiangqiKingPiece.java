package self.chess.chesspiece.xiangqi;

import self.chess.chesspiece.european.ChessPiece;
import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XiangqiKingPiece extends ChessPiece {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();

    // constructor
    public XiangqiKingPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // Return squares for valid move
    @Override
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.clear();
        // create a list of four cells around the king
        List<Cell> posCell = new ArrayList<>(
                Arrays.asList(
                        new Cell(row - 1, column),
                        new Cell(row + 1, column),
                        new Cell(row, column - 1),
                        new Cell(row, column + 1)
                )
        );
        // check each cell
        for (Cell cell : posCell) {
            // if king is South side
            if ((this.getSide() == ChessGame.Side.SOUTH)
                    // check  if cell creates facing kings situation
                    && (!checkFacingKing(cell))
                    // check if the cell is inside the three center columns and three top rows
                    && (cell.getRow() >= 0)
                    && (cell.getRow() < 3)
                    && (cell.getCol() >= 3)
                    && (cell.getCol() < 6)
                    // check if there is a piece of the same side at that cell
                    && (((board.getPiece(cell.getRow(), cell.getCol())) == null)
                    // check if there is an enemy piece at that cell
                    || (board.getPiece(cell.getRow(), cell.getCol()).getSide() != this.getSide()))) {
                possibleMoves.add(new Cell(cell.getRow(), cell.getCol()));
            }
            // if king is North side
            if ((this.getSide() == ChessGame.Side.NORTH)
                    // check if cell creates facing kings situation
                    && (!checkFacingKing(cell))
                    // check if the cell is inside the three center columns and three bottom rows
                    && (cell.getRow() >= 7)
                    && (cell.getRow() < 10)
                    && (cell.getCol() >= 3)
                    && (cell.getCol() < 6)
                    // check if there is a piece of the same side at that cell
                    && (((board.getPiece(cell.getRow(), cell.getCol())) == null)
                    // check if there is an enemy piece at that cell
                    || (board.getPiece(cell.getRow(), cell.getCol()).getSide() != this.getSide()))) {
                possibleMoves.add(new Cell(cell.getRow(), cell.getCol()));
            }
        }
        return possibleMoves;
    }

    // check if two kings might face each other
    public boolean checkFacingKing(Cell cell) {
        // count the empty cells and the king in column
        int countEmpty = 0;
        int countKing = 0;
        // check each cell in column
        // if king is south side
        if (this.getSide() == ChessGame.Side.SOUTH) {
            for (int i = this.getRow(); i < getChessBoard().numRows(); i++) {
                // check if other king is in the same column
                if (!getChessBoard().hasPiece(i, cell.getCol())) {
                    countEmpty++;
                }
                // count empty cells in the same column
                if ((getChessBoard().getPiece(i, cell.getCol()) instanceof XiangqiKingPiece)
                        && (getChessBoard().getPiece(i, cell.getCol()).getSide() != this.getSide())) {
                    countKing++;
                }
                // return true if there is a king of other side and all other cells are empty
                if ((countKing == 1) && (countEmpty == getChessBoard().numRows() - this.getRow() - 1)) {
                    return true;
                }
            }
        }
        // if king is north side
        if (this.getSide() == ChessGame.Side.NORTH) {
            for (int k = this.getRow(); k >= 0; k--) {
                // check if other king is in the same column
                if (!getChessBoard().hasPiece(k, cell.getCol())) {
                    countEmpty++;
                }
                // count empty cells in the same column
                if ((getChessBoard().getPiece(k, cell.getCol()) instanceof XiangqiKingPiece)
                        && (getChessBoard().getPiece(k, cell.getCol()).getSide() != this.getSide())) {
                    countKing++;
                }
                // return true if there is a king of other side and all other cells are empty
                if ((this.getSide() == ChessGame.Side.NORTH) && (countKing == 1) && (countEmpty == this.getRow())) {
                    return true;
                }
            }
        }

        return false;
    }

    // check if a move is legal
    @Override
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if ((possibleMove.compareTo(targetSquare) == 0)
                    && (super.isLegalMove(toRow, toColumn))) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    @Override
    public boolean isLegalNonCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (!getChessBoard().hasPiece(row, column));
    }

    // show whether a capture move is valid
    @Override
    public boolean isLegalCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // handle details after a move is done
    @Override
    public void moveDone(int row, int column) {
        if ((this.isLegalMove(row, column)) && (this.getChessBoard().hasPiece(row, column)) && (this.getChessBoard().getPiece(row, column).getSide() != this.getSide()))
            super.getChessBoard().removePiece(row, column);
        this.getChessBoard().addPiece(new XiangqiKingPiece(this.getSide(), XiangQiPieceType.X.name, this.getChessBoard()), row, column);
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }
}
