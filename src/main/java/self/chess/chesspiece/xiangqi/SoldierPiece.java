package self.chess.chesspiece.xiangqi;

import self.chess.chesspiece.european.ChessPiece;
import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;

public class SoldierPiece extends ChessPiece {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();

    // constructor
    public SoldierPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // Return squares for valid move
    @Override
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.clear();

        // check if this is the only piece between two xiangqi king
        if (super.kingFacingKing()) {
            if (this.getSide() == ChessGame.Side.NORTH) {
                possibleMoves.add(new Cell(row - 1, column));
            }
            if (this.getSide() == ChessGame.Side.SOUTH) {
                possibleMoves.add(new Cell(row + 1, column));
            }
            return possibleMoves;
        }

        switch (this.getSide()) {
            case NORTH: {
                possibleMoves.add(new Cell(row - 1, column));
                if (row < 5) {
                    possibleMoves.add(new Cell(row, column + 1));
                    possibleMoves.add(new Cell(row, column - 1));
                }
                break;
            }
            case SOUTH: {
                possibleMoves.add(new Cell(row + 1, column));
                if (row > 4) {
                    possibleMoves.add(new Cell(row, column + 1));
                    possibleMoves.add(new Cell(row, column - 1));
                }
                break;
            }
            default:
                break;
        }

        return possibleMoves;
    }

    // check if a move is legal
    @Override
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if ((possibleMove.compareTo(targetSquare) == 0)
                    && (super.isLegalMove(toRow, toColumn))) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    @Override
    public boolean isLegalNonCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (!getChessBoard().hasPiece(row, column));
    }

    // show whether a capture move is valid
    @Override
    public boolean isLegalCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // handle details after a move is done
    @Override
    public void moveDone(int row, int column) {
        if ((this.isLegalMove(row, column)) && (this.getChessBoard().hasPiece(row, column))
                && (this.getChessBoard().getPiece(row, column).getSide() != this.getSide()))
            super.getChessBoard().removePiece(row, column);
        this.getChessBoard().addPiece(new SoldierPiece(this.getSide(), XiangQiPieceType.S.name,
                this.getChessBoard()), row, column);
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }
}
