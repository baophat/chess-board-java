package self.chess.chesspiece.xiangqi;

import self.chess.chesspiece.european.ChessPiece;
import self.chess.chesspiece.european.EuropeanPieceType;
import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;
import self.chess.moving.HorizontalMove;
import self.chess.moving.VerticalMove;

import java.util.ArrayList;

public class RookPiece extends ChessPiece implements VerticalMove, HorizontalMove {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();
    // possibleVerticalMoves stores all possible vertical moves for the rook
    private final ArrayList<Cell> possibleVerticalMoves = new ArrayList<>();
    // possibleHorizontalMoves stores all possible horizontal moves for the rook
    private final ArrayList<Cell> possibleHorizontalMoves = new ArrayList<>();
    // firstMove check if this is the first move of the piece
    private boolean firstMove = true;

    //constructor
    public RookPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // return squares for vertical moves
    public ArrayList<Cell> moveVertical(ChessBoard board, int row, int column) {
        possibleVerticalMoves.clear();
        int tempRow = row - 1;
        while (tempRow >= 0) {
            if (board.getPiece(tempRow, column) == null)
                possibleVerticalMoves.add(new Cell(tempRow, column));
            else if (board.getPiece(tempRow, column).getSide() == this.getSide())
                break;
            else {
                possibleVerticalMoves.add(new Cell(tempRow, column));
                break;
            }
            tempRow--;
        }
        tempRow = row + 1;
        while (tempRow < board.numRows()) {
            if (board.getPiece(tempRow, column) == null)
                possibleVerticalMoves.add(new Cell(tempRow, column));
            else if (board.getPiece(tempRow, column).getSide() == this.getSide())
                break;
            else {
                possibleVerticalMoves.add(new Cell(tempRow, column));
                break;
            }
            tempRow++;
        }
        return possibleVerticalMoves;
    }

    // return squares for horizontal moves
    public ArrayList<Cell> moveHorizontal(ChessBoard board, int row, int column) {
        possibleHorizontalMoves.clear();
        int tempColumn = column - 1;
        while (tempColumn >= 0) {
            if (board.getPiece(row, tempColumn) == null)
                possibleHorizontalMoves.add(new Cell(row, tempColumn));
            else if (board.getPiece(row, tempColumn).getSide() == this.getSide())
                break;
            else {
                possibleHorizontalMoves.add(new Cell(row, tempColumn));
                break;
            }
            tempColumn--;
        }
        tempColumn = column + 1;
        while (tempColumn < board.numColumns()) {
            if (board.getPiece(row, tempColumn) == null)
                possibleHorizontalMoves.add(new Cell(row, tempColumn));
            else if (board.getPiece(row, tempColumn).getSide() == this.getSide())
                break;
            else {
                possibleHorizontalMoves.add(new Cell(row, tempColumn));
                break;
            }
            tempColumn++;
        }
        return possibleHorizontalMoves;
    }

    // return all possible moves
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.addAll(this.moveVertical(board, row, column));
        possibleMoves.addAll(this.moveHorizontal(board, row, column));
        return possibleMoves;
    }

    // check first move status
    public boolean getFirstMoveStatus() {
        return this.firstMove;
    }

    // set first move status
    public void setFirstMoveStatus(boolean status) {
        this.firstMove = status;
    }

    // check if a move is legal
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if (possibleMove.compareTo(targetSquare) == 0 && super.isLegalMove(toRow, toColumn)) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    public boolean isLegalNonCaptureMove(int row, int column) {
        return this.isLegalMove(row, column)
                && !getChessBoard().hasPiece(row, column);
    }

    // show whether a capture move is valid
    public boolean isLegalCaptureMove(int row, int column) {
        return this.isLegalMove(row, column)
                && getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column));
    }

    // handle details after a move is done
    public void moveDone(int row, int column) {
        if (this.isLegalMove(row, column) && this.getChessBoard().hasPiece(row, column)
                && this.getChessBoard().getPiece(row, column).getSide() != this.getSide())
            super.getChessBoard().removePiece(row, column);
        this.getChessBoard().addPiece(
                new RookPiece(this.getSide(), EuropeanPieceType.R.name, this.getChessBoard()), row, column);
        ((RookPiece) this.getChessBoard().getPiece(row, column)).setFirstMoveStatus(false);
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }
}
