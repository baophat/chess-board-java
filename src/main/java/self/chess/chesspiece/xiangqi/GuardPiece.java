package self.chess.chesspiece.xiangqi;

import self.chess.chesspiece.european.ChessPiece;
import self.chess.core.Cell;
import self.chess.core.ChessBoard;
import self.chess.core.ChessGame;

import java.util.ArrayList;

public class GuardPiece extends ChessPiece {
    // possibleMoves stores all possible move for the piece
    private final ArrayList<Cell> possibleMoves = new ArrayList<>();

    // constructor
    public GuardPiece(ChessGame.Side side, String label, ChessBoard board) {
        super(side, label, board);
    }

    // Return squares for valid move
    @Override
    public ArrayList<Cell> move(ChessBoard board, int row, int column) {
        possibleMoves.clear();

        // check if this is the only piece between two xiangqi king
        if (super.kingFacingKing()) {
            return possibleMoves;
        }

        switch (this.getSide()) {
            case SOUTH: {
                // Guards of South side
                if (this.getRow() != 1 && this.getColumn() != 4) {
                    possibleMoves.add(new Cell(1, 4));
                } else {
                    calculate4DirectionsAvailableMoves(this.getRow(), this.getColumn());
                }
                break;
            }
            case NORTH: {
                // Guards of North side
                if (this.getRow() != 8 && this.getColumn() != 4) {
                    possibleMoves.add(new Cell(8, 4));
                } else {
                    calculate4DirectionsAvailableMoves(this.getRow(), this.getColumn());
                }
                break;
            }
            default:
                break;
        }
        return possibleMoves;
    }

    // check if a move is legal
    @Override
    public boolean isLegalMove(int toRow, int toColumn) {
        Cell targetSquare = new Cell(toRow, toColumn);
        move(this.getChessBoard(), this.getRow(), this.getColumn());
        for (Cell possibleMove : possibleMoves) {
            if ((possibleMove.compareTo(targetSquare) == 0)
                    && (super.isLegalMove(toRow, toColumn))) return true;
        }
        return false;
    }

    // show whether a non-capture move is valid
    @Override
    public boolean isLegalNonCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (!getChessBoard().hasPiece(row, column));
    }

    // show whether a capture move is valid
    @Override
    public boolean isLegalCaptureMove(int row, int column) {
        return (this.isLegalMove(row, column))
                && (getChessBoard().squareThreatened(row, column, getChessBoard().getPiece(row, column)));
    }

    // handle details after a move is done
    @Override
    public void moveDone(int row, int column) {
        if ((this.isLegalMove(row, column)) && (this.getChessBoard().hasPiece(row, column)) && (this.getChessBoard().getPiece(row, column).getSide() != this.getSide()))
            super.getChessBoard().removePiece(row, column);
        this.getChessBoard().addPiece(new GuardPiece(this.getSide(), XiangQiPieceType.G.name, this.getChessBoard()), row, column);
        this.getChessBoard().removePiece(this.getRow(), this.getColumn());
    }

    private void calculate4DirectionsAvailableMoves(int row, int col) {
        // check top right
        if (!super.getChessBoard().hasPiece(row - 1, col + 1)
                || super.getChessBoard().getPiece(row - 1, col + 1).getSide() != this.getSide()) {
            possibleMoves.add(new Cell(row - 1, col + 1));
        }
        // check top left
        if (!super.getChessBoard().hasPiece(row - 1, col - 1)
                || super.getChessBoard().getPiece(row - 1, col - 1).getSide() != this.getSide()) {
            possibleMoves.add(new Cell(row - 1, col - 1));
        }
        // check bottom right
        if (!super.getChessBoard().hasPiece(row + 1, col + 1)
                || super.getChessBoard().getPiece(row + 1, col + 1).getSide() != this.getSide()) {
            possibleMoves.add(new Cell(row + 1, col + 1));
        }
        // check bottom left
        if (!super.getChessBoard().hasPiece(row + 1, col - 1)
                || super.getChessBoard().getPiece(row + 1, col - 1).getSide() != this.getSide()) {
            possibleMoves.add(new Cell(row + 1, col - 1));
        }
    }
}
