package self.chess.moving;

import self.chess.core.Cell;
import self.chess.core.ChessBoard;

import java.util.ArrayList;

public interface VerticalMove {

    ArrayList<Cell> moveVertical(ChessBoard board, int row, int column);
}
