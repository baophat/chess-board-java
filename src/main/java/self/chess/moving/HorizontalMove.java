package self.chess.moving;

import self.chess.core.Cell;
import self.chess.core.ChessBoard;

import java.util.ArrayList;

public interface HorizontalMove {

    ArrayList<Cell> moveHorizontal(ChessBoard board, int row, int column);
}
