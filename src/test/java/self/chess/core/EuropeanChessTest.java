package self.chess.core;

import org.junit.Test;
import self.chess.chesspiece.european.KingPiece;
import self.chess.chesspiece.european.PawnPiece;
import self.chess.chesspiece.european.RookPiece;
import self.chess.core.european.EuropeanChess;
import self.chess.core.european.EuropeanChessDisplay;

import static org.junit.Assert.assertTrue;

public class EuropeanChessTest {

    @Test
    public void testMoveDoneForCastleMove() {
        // create the chess board for test
        EuropeanChess ec = new EuropeanChess();
        EuropeanChessDisplay ecd = new EuropeanChessDisplay();
        ChessBoard board = new SwingChessBoard(ecd, ec);

        // create king, rook, and pawn, and add all to board
        KingPiece kingWhite = new KingPiece(ChessGame.Side.NORTH, "King", board);
        board.addPiece(kingWhite, 7, 4);
        RookPiece rookWhite = new RookPiece(ChessGame.Side.NORTH, "Rook", board);
        board.addPiece(rookWhite, 7, 7);
        PawnPiece pawnWhite = new PawnPiece(ChessGame.Side.NORTH, "Pawn", board);
        board.addPiece(pawnWhite, 6, 4);

        assertTrue("Check if other move is successful", ec.makeMove(pawnWhite, 5, 4));
    }

    @Test
    public void testMoveDoneForOtherMove() {
        // create the chess board for test
        EuropeanChess ec = new EuropeanChess();
        EuropeanChessDisplay ecd = new EuropeanChessDisplay();
        ChessBoard board = new SwingChessBoard(ecd, ec);

        // create king, rook, and pawn, and add all to board
        KingPiece kingWhite = new KingPiece(ChessGame.Side.NORTH, "King", board);
        board.addPiece(kingWhite, 7, 4);
        RookPiece rookWhite = new RookPiece(ChessGame.Side.NORTH, "Rook", board);
        board.addPiece(rookWhite, 7, 7);
        PawnPiece pawnWhite = new PawnPiece(ChessGame.Side.NORTH, "Pawn", board);
        board.addPiece(pawnWhite, 6, 4);

        assertTrue("Check if castle move is successful", ec.makeMove(kingWhite, 7, 6));
    }
}
